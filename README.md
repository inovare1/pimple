## Introduction

Pimple is an e-commerce framework built for [Laravel](https://laravel.com). This provides a way to lessen the time in building online stores.

### Requirements

The minimum Laravel version required is v5.8.

### Installation

To get started, use Composer to install Pimple into your Laravel project:

```bash
composer require pimple/pimple
```

After that, publish it's config file and assets (JS/CSS and images):

```bash
php artisan pimple:install
```
